def create_menu() :
    '''
    Créer un menu vide
    '''
    menu = []
    return menu

def add_in_menu(menu, texte, commande) :
    '''
    Créé un tuple qui associe la commande et la fonction
    '''
    menu.append((texte, commande))

def afficher_menu(menu) :
    '''
    Afficher un menu avec la commande et un indice associé
    '''
    for i, v in enumerate(menu) :
        print(i + 1, '. ', v[0], sep = '')

### Exceptions :

class LaboException(Exception):
    """ Généralise les exceptions du laboratoire."""
    pass


class AbsentException(LaboException):
    pass


class PresentException(LaboException):
    pass

def nouveau_labo() :
    '''
    Créer un nouveau labo
    '''
    dico = {}
    return dico

def enregistrer_arrivee(labo, nom, bureau) :
    '''
    Enregistrer un nouvel arrivé
    '''
    if nom in labo :
        raise PresentException(nom)
    
    labo[nom] = bureau

def enregistrer_depart(labo, nom) :
    '''
    Enregistrer le départ d'une personne
    '''
    if nom not in labo : 
        raise AbsentException(nom)
    
    del labo[nom]

def change_bureau(labo, nom, new_bureau) :
    '''
    Changer de bureau
    '''
    if nom not in labo : 
        raise AbsentException(nom)

    labo[nom] = new_bureau
    return labo

def change_nom(labo, old_nom, new_nom, bureau) :
    '''
    Changer de nom
    '''
    if old_nom not in labo : 
        raise AbsentException(nom)

    del labo[old_nom]
    labo[new_nom] = bureau
    return labo

def is_membre(labo, nom) :
    '''
    Savoir si une personne est membre du laboratoire
    '''
    is_bool = False
    if nom in labo :
        is_bool = True
    else :
        is_bool = False
    return is_bool

def get_bureau(labo, nom) :
    '''
    Obtenir le bureau
    '''
    if nom not in labo : 
        raise AbsentException(nom)
    
    return labo[nom]

def list_labo(labo) :
    '''
    Lister le labo
    '''
    return labo.items()


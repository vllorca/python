import labo
import menu as m
import csv 
import json

def show_menu(menu) : 
    '''
    Afficher le menu
    '''
    print('')
    print('MENU : \n')
    m.afficher_menu(menu)
    print('')

def demander_choix() :
    '''
    Demander le choix de l'utilisateur
    '''
    user_choice = int(input('Que voulez vous faire : '))
    assert 0 <= user_choice <= 8

    return user_choice

def flip_dict(labo, flipped) :
    '''
    Flip un dictionnaire entre les valeurs et les clés
    '''
    for key, value in labo.items():
        if value not in flipped :
            flipped[value] = [key]
        else: 
            flipped[value].append(key) 
    return flipped

def write_file(filename_txt, filename_html, labo) :
    '''
    Liste et affiche le labo dans un fichier texte et une page html
    '''
    text_file = open(filename_txt, "w")
    html_file = open(filename_html,"w")
    
    flipped = {}
    flipped = flip_dict(labo, flipped)

    for key, value in flipped.items():
        text_file.write(f'{key} :\n')
        html_file.write(f'<th> {key} </th> <br>\n')
        for v in value :
            text_file.write(f'  - {v}\n')
            html_file.write(f'<td> - {v} </td> <br>\n')

    text_file.close()
    html_file.close()

def main() :
    l = labo.nouveau_labo()

    # Opening JSON file 
    with open('labo.json') as json_file: 
        l = json.load(json_file) 

    # Opening CSV file
    with open('labo.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            try :
                labo.enregistrer_arrivee(l, row['Nom'], row['Bureau'])
            except labo.PresentException :
                print('')
                print(f"Le numéro du Bureau a changé pour {row['Nom']}")
    
    # Arrivé nouvelle personne
    def choix_1() :
        nom = input('Nom de l\'arrivant : ')
        bureau = input('Bureau de l\'arrivant : ')

        try :      
            labo.enregistrer_arrivee(l, nom, bureau)  
        except labo.PresentException :
            print("L'arrivant est déjà présent dans le labo")

    # Départ personne
    def choix_2() :
        nom = input('Nom de l\'arrivant : ')
        
        try :       # Exception : Le nom n'est pas dans le liste
            labo.enregistrer_depart(l, nom)  
        except labo.AbsentException :
            print('Le nom n\'est pas dans la liste')

    #Modifier bureau
    def choix_3() :
        nom = input('Nom pour le changement : ')
        new_bureau = input('Nouveau bureau : ')
        try :
            labo.change_bureau(l, nom, new_bureau)        
        except labo.AbsentException :
            print('Erreur')

    # Changer le nom
    def choix_4() :
        old_nom = input('Nom à changer : ')
        new_nom = input('Nouveau nom : ')
        try :
            labo.change_nom(l, old_nom, new_nom, l[old_nom])
        except labo.AbsentException :
            print('Erreur')

    # Savoir si une personne est membre du laboratoire
    def choix_5() :
        nom = input("Entrez le nom : ")
        if labo.is_membre(l, nom) :
            print(f'{nom} est dans le labo')
        else : 
            print(f'{nom} n\'appartient pas au labo')

    # Obtenir le bureau
    def choix_6() :
        nom = input("Entrez le nom : ")
        try :
            print(f'{nom} occupe le bureau {labo.get_bureau(l, nom)}')
        except labo.AbsentException :
            print('Erreur')

    # Lister le labo
    def choix_7() :
        print(labo.list_labo(l))
    
    # Lister le labo dans un fichier texte
    def choix_8() :
        write_file('labo.txt', 'labo.html', l)

    # Quitter
    def choix_0() :
        print("Labo saved ..")
        print("Bye bye")

        with open('labo.json', 'w') as fd:
            json.dump(l, fd)

    # Créer le menu
    menu = m.create_menu()

    # Associer les commandes aux fonctions associées
    m.add_in_menu(menu, 'Enregistrer l\'arrivée d\'une nouvelle personne', choix_1)
    m.add_in_menu(menu, 'Enregistrer le départ d\'une personne', choix_2)
    m.add_in_menu(menu, 'Modifier le bureau occupé par une personne', choix_3)
    m.add_in_menu(menu, 'Changer le nom d\'une personne au laboratoire', choix_4)
    m.add_in_menu(menu, 'Savoir si une personne est membre du laboratoire', choix_5)
    m.add_in_menu(menu, 'Obtenir le bureau d\'une personne', choix_6)
    m.add_in_menu(menu, 'Produire listing personnel', choix_7)
    m.add_in_menu(menu, 'Lister le labo dans un fichier texte', choix_8)
    m.add_in_menu(menu, 'Quitter', choix_0)

    # Afficher le menu
    show_menu(menu)

    # Choix de l'utilisateur
    while True :
        user_choice = demander_choix()
        menu[user_choice-1][1]()



if __name__ == "__main__":
        main()  


    
from compte import CompteSimple 
from compte import CompteCourant

### Test CompteSimple
#Test méthode crédit
def test_credit_cs():
    compte = CompteSimple(10, 'Vivien', 1001)
    compte.credit(100)
    assert compte.solde == 110

#Test méthode débit
def test_debit_cs():
    compte = CompteSimple(100, 'Vivien', 1001)
    compte.debit(50)
    assert compte.solde == 50

#Test méthode Débit neg
def test_debit_neg_cs():
    compte = CompteSimple(20, 'Vivien', 1001)
    compte.debit(50)
    assert compte.solde == -30


### Test CompteCourant
#Test credit
def test_credit_cc():
    compte = CompteCourant(10, 'Vivien', 1001)
    compte.credit(100)
    assert compte.solde == 110

#Test débit
def test_debit_cc():
    compte = CompteCourant(100, 'Vivien', 1001)
    compte.debit(50)
    assert compte.solde == 50

#Test edit
def test_debit_neg_cc():
    compte = CompteCourant(20, 'Vivien', 1001)
    compte.debit(50)
    assert compte.solde == -30

test_credit_cs()
test_debit_cs()
test_debit_neg_cs()

test_credit_cc()
test_debit_cc()
test_debit_neg_cc()
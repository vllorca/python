from banque import Banque
from compte import CompteSimple, CompteCourant

#test ouverture Compte Simple
def test_open_accountSimple() :
    banque = Banque()
    banque.open_accountSimple(10, 'vivien')
    assert len(banque.comptes) != 0

#Test total solde
def test_total_solde() :
    t_solde = 0
    banque = Banque()
    banque.open_accountSimple(10, 'vivien')
    banque.open_accountCourant(50, 'vivien')

    t_solde = banque.total_solde()
    assert t_solde == 60

#Test prelevement
def test_prelever(frais) :
    t_solde = 0
    banque = Banque()

    banque.open_accountSimple(100, 'vivien')
    banque.open_accountCourant(560, 'vivien')

    banque.prelever(frais)
    
    assert banque.comptes[0].solde == 50
    assert banque.comptes[1].solde == 510

#Test ouverture compteCourant
def test_open_accountCourant() :
    banque = Banque()
    banque.open_accountCourant(10, 'vivien')
    assert len(banque.comptes) != 0

#Test attribution numéro de compte par la banque
def test_num() :
    banque = Banque()
    banque.open_accountSimple(10, 'vivien')
    banque.open_accountCourant(50, 'vivien')
    assert banque.comptes[0].numero_compte == 1001
    assert banque.comptes[1].numero_compte == 1002



test_open_accountSimple()
test_open_accountCourant() 
test_total_solde()
test_prelever(50)
test_num()
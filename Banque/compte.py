class CompteSimple:
    '''
    Classe Compte Simple
    '''
    def __init__(self, solde, titulaire, numero_compte):
        self.__solde = solde
        self.titulaire = titulaire
        self.numero_compte = numero_compte

    @property
    def solde(self):
        return self.__solde
    
    # Créditer le compte
    def credit(self, montant):
        self.__solde += montant

    # Débiter le compte
    def debit(self, montant):
        self.__solde -= montant

    def __str__(self):
        return 'Le solde du compte de {} est : {} euros'.format(self.titulaire, self.solde)


class CompteCourant(CompteSimple):
    '''
    Classe Compte Courant
    '''
    def __init__(self, solde, titulaire, numero_compte) :
        super().__init__(solde, titulaire, numero_compte)
        self.historique = []

    def __str__(self):
        return f"{super().__str__()} \n{self.historique}"
    
    # Créditer le compte
    def credit(self, montant) :
        super().credit(montant)
        self.historique.append(montant)

    # Débiter le compte
    def debit(self, montant) :
        super().debit(montant)
        self.historique.append(-1*montant)
    
    # Afficher l'ensemble des débits/crédits
    def show(self) :
        print(self.historique)


    
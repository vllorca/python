from compte import CompteSimple
from compte import CompteCourant

class Banque :
    def __init__(self):
        self.comptes = []
        self.numero_compte = 1001

    #Ouvrir un compte simple
    def open_accountSimple(self, depot, titulaire):
        compte = CompteSimple(depot, titulaire, self.numero_compte)
        self.comptes.append(compte)
        self.numero_compte += 1
    
    #Ouvrir un compte courant
    def open_accountCourant(self, depot, titulaire):
        compte = CompteCourant(depot, titulaire, self.numero_compte)
        self.comptes.append(compte)
        self.numero_compte += 1

    # Afficher le solde total
    def total_solde(self):
        total_solde = 0
        for c in self.comptes :
            total_solde += c.solde
        return total_solde
    
    # Prélever les frais de la banque
    def prelever(self, frais):
        for c in self.comptes :
            c.debit(frais)

    # Editer l'historique d'un compte
    def edit(self, compte, indice, valeur) :
        compte.historique[indice] = valeur

